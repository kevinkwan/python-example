"""
082
Show the user a line of text from your favourite poem and ask for a starting and ending point. Display the characters between those two points.
"""
poem = "Because I could not stop for Death"
print(poem)

starting = int(input("Enter starting point: "))
ending = int(input("Enter ending point: "))

# after_cut = []
# for i in range(starting, ending):
# 	after_cut.append(poem[i])

# for i in after_cut:
# 	print(i, end="")

print(poem[starting:ending])