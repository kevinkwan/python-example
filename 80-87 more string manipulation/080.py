"""
080
Ask the user to enter their first name and then display the length of their first name. Then ask for their surname and display the length of their surname. Join their first name and surname together with a space between and display the result. Finally, display the length of their full name (including the space).
"""
user_first_name = str(input("Enter your first name: "))
print(f"Your length of your first name is {len(user_first_name)}")

user_surname = str(input("Enter your surname: "))
fullname = f"{user_first_name} {user_surname}"
print("Your full name is", fullname)

print(f"The length of your full name is {len(fullname)}")