"""
072
Create a list of six school subjects. Ask the user which of these subjects they don’t like. Delete the subject they have chosen from the list before you display the list again.
"""
school_subjects = ["Mathematics", "Science", "Handcrafts", "English"]

print(school_subjects)

student_dislike = str(input("Which subject you don't like?\n>"))
school_subjects.remove(student_dislike)
print(school_subjects)