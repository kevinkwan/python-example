"""
077
Change program 076 so that once the user has completed their list of names, display the full list and ask them to type in one of the names on the list. Display the position of that name in the list. Ask the user if they still want that person to come to the party. If they answer “no”, delete that entry from the list and display the list again.
"""
people = []
for i in range(2):
	ask_people_name = str(input("Enter people name: "))
	people.append(ask_people_name)
	print(ask_people_name, "has been added")
again = True
while again == True:
	ask_people_name = str(input("Enter people name: "))
	people.append(ask_people_name)
	print(ask_people_name, "has been added")
	ask_again = str(input("Want to add another? (Y/N): "))
	if ask_again == "Y":
		again = True
	else:
		again = False

print(people)

user_input = str(input("Enter a people name in list: "))
print("The people name index is", people.index(user_input))
invite = str(input("Do you still invite that people? (yes/no): "))
if invite == "no":
	people.remove(user_input)

print(people)