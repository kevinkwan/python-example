"""
074
Enter a list of ten colours. Ask the user for a starting number between 0 and 4 and an end number between 5 and 9. Display the list for those colours between the start and end numbers the user input.
"""
colors = ["Aero", "Amber", "Aqua", "Azure", "Black", "Brown", "Cadet", "Carmine", "Citron", "Coffee"]
print(colors)

start = int(input("Enter starting number:\n>"))
end = int(input("Enter end number:\n>"))

print(colors[start:end])