"""
079
Create an empty list called “nums”. Ask the user to enter numbers. After each number is entered, add it to the end of the nums list and display the list. Once they have entered three numbers, ask them if they still want the last number they entered saved. If they say “no”, remove the last item from the list. Display the list of numbers.
"""

nums = []
for i in range(3):
	enter_num = int(input("Enter a number: "))
	nums.append(enter_num)

save = str(input("Save the last number into list? (yes/no): "))
if save == "no":
	nums = nums[:-1]

print(nums)