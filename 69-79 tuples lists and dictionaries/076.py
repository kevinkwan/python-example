"""
076
Ask the user to enter the names of three people they want to invite to a party and store them in a list. After they have entered all three names, ask them if they want to add another. If they do, allow them to add more names until they answer “no”. When they answer “no”, display how many people they have invited to the party.
"""
people = []
for i in range(2):
	ask_people_name = str(input("Enter people name: "))
	people.append(ask_people_name)
	print(ask_people_name, "has been added")
again = True
while again == True:
	ask_people_name = str(input("Enter people name: "))
	people.append(ask_people_name)
	print(ask_people_name, "has been added")
	ask_again = str(input("Want to add another? (Y/N): "))
	if ask_again == "Y":
		again = True
	else:
		again = False

print(people)