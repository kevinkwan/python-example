"""
071
Create a list of two sports. Ask the user what their favourite sport is and add this to the end of the list. Sort the list and display it.
"""
sport_list = ["Soccer", "Basketball"]

user_sport = str(input("What is your favourite sport?\n>"))

sport_list.append(user_sport)

print(sorted(sport_list))