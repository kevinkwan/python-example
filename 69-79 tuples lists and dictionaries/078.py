"""
078
Create a list containing the titles of four TV programmes and display them on separate lines. Ask the user to enter another show and a position they want it inserted into the list. Display the list again, showing all five TV programmes in their new positions.
"""
tv_programmes = ["Market Update", "Inside Market", "CGTN Global Watch[Eng]", "Putonghua News Report"]

for i in tv_programmes:
	print(i)

new_tv = str(input("Enter another show: "))
new_pos = int(input("Enter the position: "))

tv_programmes.insert(new_pos, new_tv)

for i in tv_programmes:
	print(i)
	