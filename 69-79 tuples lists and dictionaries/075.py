"""
075
Create a list of four three-digit numbers. Display the list to the user, showing each item from the list on a separate line. Ask the user to enter a three-digit number. If the number they have typed in matches one in the list, display the position of that number in the list, otherwise display the message “That is not in the list”.
"""
num_list = [123, 456, 789, 999]
for i in num_list:
	print(i)

user_num = int(input("Enter a three_digit number:\n>"))

if user_num in num_list:
	print(f"You number of the position is {num_list.index(user_num)}")
else:
	print("That is not in the list")