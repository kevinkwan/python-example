"""
069
Create a tuple containing the names of five countries and display the whole tuple. Ask the user to enter one of the countries that have been shown to them and then display the index number (i.e. position in the list) of that item in the tuple.
"""
country_tuple = ("Canada", "Japan", "Germany", "Switzerland", "Australia")
print(country_tuple)

user_enter = str(input("Which country to check:\n>"))
if user_enter in country_tuple:
	print(country_tuple.index(user_enter))
else:
	print("Data not found.")