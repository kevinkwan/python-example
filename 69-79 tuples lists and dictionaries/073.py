"""
073
Ask the user to enter four of their favourite foods and store them in a dictionary so that they are indexed with numbers starting from 1. Display the dictionary in full, showing the index number and the item. Ask them which they want to get rid of and remove it from the list. Sort the remaining data and display the dictionary.
"""
food_dic = {}
for i in range(1, 5):
    food_dic[i] = str(input("Enter your food:"))
print(food_dic)

get_rid = int(input("Which one do you glad to remove? :"))
del food_dic[get_rid]
print(food_dic)
print(sorted(food_dic))
print(sorted(food_dic.values()))
