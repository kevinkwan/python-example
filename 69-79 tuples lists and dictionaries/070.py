"""
070
Add to program 069 to ask the user to enter a number and display the country in that position.
"""
country_tuple = ("Canada", "Japan", "Germany", "Switzerland", "Australia")
print(country_tuple)

user_enter = str(input("Which country to check:\n>"))
if user_enter in country_tuple:
    print(country_tuple.index(user_enter))
else:
    print("Data not found.")

user_num = int(input("Enter the number to display country:\n>"))
print(f"The position of the country is {country_tuple[user_num]}")
