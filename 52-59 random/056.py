"""
056
Randomly pick a whole number between 1 and 10. Ask the user to enter a number and keep entering numbers until they enter the number that was randomly picked.
"""
import random

ran_num = random.randint(1, 10)
usr_num = 0
while usr_num != ran_num:
    usr_num = int(input("Enter a number:"))

print("Your number same as randomly pick.")