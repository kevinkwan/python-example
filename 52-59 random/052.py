"""
052
Display a random integer between 1 and 100 inclusive.
"""
import random

random_integer = random.randint(1, 100)
print(random_integer)