"""
058
Make a maths quiz that asks five questions by randomly generating two whole numbers to make the question (e.g. [num1] + [num2]). Ask the user to enter the answer. If they get it right add a point to their score. At the end of the quiz, tell them how many they got correct out of five.
"""
import random

questions = 5
score = 0
for x in range(questions):
    num1 = random.randint(1, 10)
    num2 = random.randint(1, 10)
    total = num1 + num2
    answer = int(input(f"{num1} + {num2} = ?\n>"))
    if answer == total:
        score += 1
print("Finaly your got correct", score, "of five.")
