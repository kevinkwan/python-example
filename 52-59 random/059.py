"""
059
Display five colours and ask the user to pick one. If they pick the same as the program has chosen, say “Well done”, otherwise display a witty answer which involves the correct colour, e.g. “I bet you are GREEN with envy” or “You are probably feeling BLUE right now”. Ask them to guess again; if they have still not got it right, keep giving them the same clue and ask the user to enter a colour until they guess it correctly.
"""
import random

colors = ["black", "white", "red", "yellow", "blue"]

program_pick = random.choice(colors)

for x in colors:
    print(str.title(x))

start_again = True
while start_again == True:
    user_pick = str.lower(input("Choose your color:\n>"))
    if user_pick == program_pick:
        print("Well done")
        start_again = False
    else:
        if user_pick == "black":
            print("I bet you are Black with envy.")
        if user_pick == "white":
            print("You are probably feeling White right now")
        if user_pick == "red":
            print("I bet you are Red with envy.")
        if user_pick == "yellow":
            print("You are probably feeling Yellow right now")
        if user_pick == "blue":
            print("I bet you are Blue with envy.")
