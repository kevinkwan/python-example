"""
057
Update program 056 so that it tells the user if they are too high or too low before they pick again.
"""
import random

ran_num = random.randint(1, 10)
usr_num = int(input("Pick a number:"))
while usr_num != ran_num:
    if usr_num > ran_num:
        print("Too high")
    elif usr_num < ran_num:
        print("Too low")
    usr_num = int(input("Pick again"))

print("Your number same as randomly pick.")