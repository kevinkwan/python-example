"""
054
Randomly choose either heads or tails (“h” or “t”). Ask the user to make their choice. If their choice is the same as the randomly selected value, display the message “You win”, otherwise display “Bad luck”. At the end, tell the user if the computer selected heads or tails.
"""
import random

select = ["h", "t"]
cpu_select = random.choice(select)
user_select = str(input("Enter your choice.\n>"))
if str.lower(user_select) == cpu_select:
	print("You win")
else:
	print("Bad luck")
	print("The computer selected", cpu_select)