"""
055
Randomly choose a number between 1 and 5. Ask the user to pick a number. If they guess correctly, display the message “Well done”, otherwise tell them if they are too high or too low and ask them to pick a second number. If they guess correctly on their second guess, display “Correct”, otherwise display “You lose”.
"""
import random

random_pick = random.randint(1, 5)
user_pick = int(input("Pick a number between 1 and 5:\n"))
if user_pick > random_pick:
    print("Too high")
    user_second_pick = int(input("Pick a second number?\n>"))
    if user_second_pick != random_pick:
        print("You lose")
    else:
        print("Correct")
elif user_pick < random_pick:
    print("Too low")
    user_second_pick = int(input("Pick a second number?\n>"))
    if user_second_pick != random_pick:
        print("You lose")
    else:
        print("Correct")
else:
    print("Well done")

