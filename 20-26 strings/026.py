"""
026
Pig Latin takes the first consonant of a word, moves it to the end of the word and adds on an “ay”. If a word begins with a vowel you just add “way” to the end. For example, pig becomes igpay, banana becomes ananabay, and aadvark becomes aadvarkway. Create a program that will ask the user to enter a word and change it into Pig Latin. Make sure the new word is displayed in lower case.
"""
user_word = str(input("Please enter a word.\n>"))
first_letter = str.lower(user_word[0])
aeiou = ['a', 'e', 'i', 'o', 'u']
if first_letter in aeiou:
	print(str.lower(user_word) + "way")
else:
	word_length = len(user_word)
	new_user_word = user_word[1:word_length]
	print(str.lower(new_user_word) + first_letter + "ay")