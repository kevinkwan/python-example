"""
023
Ask the user to type in the first line of a nursery rhyme and display the length of the string. Ask for a starting number and an ending number and then display just that section of the text (remember Python starts counting from 0 and not 1).
"""
line = str(input(f"Please enter first line of a nursery rhyme.\n>"))
print(f"The length of your inputs is {len(line)}")

start = int(input("What is the starting number?\n>"))
end = int(input("What is the ending number?\n>"))

print(line[start:end])