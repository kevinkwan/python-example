"""
021
Ask the user to enter their first name and then ask them to enter their surname. Join them together with a space between and display the name and the length of whole name.
"""
first_name = str(input(f"Please enter your first name.\n>"))
surname = str(input(f"then enter your surname.\n>"))
full_name = first_name + " " + surname
print(
    f"Here is your full name '{full_name}' and the length is {len(full_name)}")
