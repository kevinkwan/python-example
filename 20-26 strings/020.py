"""
020
Ask the user to enter their first name and then display the
length of their name.
"""
first_name = str(input(f"Please enter your first name\n>"))
print(f"The length of your first name is {len(first_name)}")
