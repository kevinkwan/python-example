"""
022
Ask the user to enter their first name and surname in lower case. Change the case to title case and join them together. Display the finished result.
"""
first_name = str(input(f"Enter your first name.\n>"))
surname = str(input(f"Enter your surname.\n>"))
fullname = first_name + " " + surname
print(fullname.title())
