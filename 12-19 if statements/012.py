"""
012
Ask for two numbers. If the first one is larger than the second, display the second number first and then the first number, otherwise show the first number first and then the second.
"""
first_num = int(input(f"Enter your first number: "))
second_num = int(input(f"Enter your second number: "))
if first_num > second_num:
    print(second_num, first_num)
else:
    print(first_num, second_num)
