"""
011
Task the user to enter a number over 100 and then enter a number under 10 and tell them how many times the smaller number goes into the larger number in a user-friendly format.
"""
big_num = int(input(f"Enter a number over 100:"))
small_num = int(input(f"Enter a number under 10:"))
answer = big_num // small_num
print("There are", answer, "times the smaller number goes into the larger number.")
