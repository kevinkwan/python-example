"""
007
Ask the user for their name and their age. Add 1 to their age and display the output [Name] next birthday you will be [new age].
"""
user_name = input(f"What is your name? :")
user_age = int(input(f"How old are you? :"))
user_age+=1
print(user_name, "next birthday you will be", user_age)