"""
008
Ask for the total price of the bill, then ask how many diners there are. Divide the total bill by the number of diners and show how much each person must pay.
"""
total_bill = int(input(f"What is the price of the bill? :"))
total_player = int(input(f"How many people are there? :"))
each_person_pay = total_bill / total_player
print("Each person must pay $", int(each_person_pay))