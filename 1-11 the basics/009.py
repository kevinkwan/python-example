"""
009
Write a program that will ask for a number of days and then will show how many hours, minutes and seconds are in that number of days.
"""
days = int(input(f"Enter a number of days: "))
hours = days * 24
minutes = hours * 60
seconds = minutes * 60
print("There will be", hours, "hours in that number of days.")
print("There will be", minutes, "minutes in that number of days.")
print("There will be", seconds, "seconds in that number of days.")
