"""
006
Ask how many slices of pizza the user started with and ask how many slices they have eaten. Work out how many slices they have left and display the answer in a user- friendly format.
"""
number1 = int(input(f"How many slices of pizza do you have? :"))
number2 = int(input(f"How many slices of pizza have you eaten? :"))
answer = number1 - number2
print("You have", answer, "slices of pizza left.")