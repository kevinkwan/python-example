"""
032
Ask for the radius and the depth of a cylinder and work out the total volume (circle area*depth) rounded to three decimal places.
"""
import math
radius = float(input("Going to calculat the total volume of a cylinder.\n Please enter radius:\n>"))
depth = float(input("Please also enter depth:\n>"))
circle_area = math.pi * math.sqrt(radius)
total_volume = circle_area * depth
print(f"The total volume result is :{round(total_volume, 3)}")