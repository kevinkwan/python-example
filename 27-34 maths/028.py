"""
028
Update program 027 so that it will display the answer to two decimal places.
"""
number = float(input("Please enter a number with lots of decimal.\n>"))
number = number * 2
print(round(number, 2))