"""
Display the following message:
1) Square
2) Triangle

Enter a number:
If the user enters 1, then it should ask them for the area one of its sides and display the area. If they select 2, it should ask for the base and height of the triangle and display the area. If they type in anything else, it should give them a suitable error message.
"""
user_choice = int(input("""1) Square
2) Triangle

Enter a number:"""))

if user_choice == 1:
    length = int(input("Please enter the length:\n>"))
    area = length ** 2
    print(f"The square area should be {area}")
elif user_choice == 2:
    base = int(input("Please enter the base:\n>"))
    height = int(input("Please also enter the height:\n>"))
    area = base * height / 2
    print(f"The Triangle area should be {area}")
else:
    print("error..")
