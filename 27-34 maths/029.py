"""
029
Ask the user to enter an integer that is over 500. Work out the square root of that number and display it to two decimal places.
"""
import math
user_input = int(input("Enter a integer that is over 500:\n>"))
new_user_input = math.sqrt(user_input)
print(round(new_user_input, 2))