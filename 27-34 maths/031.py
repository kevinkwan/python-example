"""
031
Ask the user to enter the radius of a circle (measurement from the centre point to the edge). Work out the area of the circle (π*radius2).
"""
import math

radius = float(input("Please enter a radius for a circle:\n>"))
text = "The area of the circel should be:"
area = round(math.pi * math.sqrt(radius), 2)
print(text, area)
