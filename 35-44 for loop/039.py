"""
039
Ask the user to enter a number between 1 and 12 and then display the times table for that number.
"""
number = int(input("Enter a number between 1 and 12:"))
for x in range(1, 13):
	print(f"{x} * {number} = {x*number}")