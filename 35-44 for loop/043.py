"""
043
Ask which direction the user wants to count (up or down). If they select up, then ask them for the top number and then count from 1 to that number. If they select down, ask them to enter a number below 20 and then count down from 20 to that number. If they entered something other than up or down, display the message “I don’t understand”.
"""
action = str(input("Which direction do you want? (Up/Down):"))
if str.lower(action) == "up":
	number = int(input("Enter the top number:"))
	for x in range(1, number+1):
		print(x)
elif str.lower(action) == "down":
	number = int(input("Enter a number below 20:\n>"))
	if number < 20:
		for x in range(number, 0, -1):
			print(x)
	else:
		print("I don't understand")
else:
	print("I don't understand")