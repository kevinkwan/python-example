"""
044
Ask how many people the user wants to invite to a party. If they enter a number below 10, ask for the names and after each name display “[name] has been invited”. If they enter a number which is 10 or higher, display the message “Too many people”.
"""
num_of_people = int(input("How many people invite?\n>"))
if num_of_people < 10:
	for x in range(num_of_people, 0, -1):
		name = str(input("Enter the people name to invite:\n>"))
		print(f"{name} has been invited.")
else:
	print("Too many people")