"""
040
Ask for a number below 50 and then count down from 50 to that number, making sure you show the number they entered in the output.
"""
number = int(input("Enter a number below 50:\n>"))
print("Start to count down form", number)
for x in range(number, 0, -1):
	print(x)